/********************************************************************************************************
/* Author : Joe Cris Molina
/* Revision History : 
/*       June 20, 2018 (created)
/* Description :
/*  Get the MEAN, MEDIAN and MODE of an input data set
*********************************************************************************************************/

/*******************************************************************
/ getMean
/ MEAN - is defined as the average or is equal to the sum of
/ all elements divided by the total number of elements 
*******************************************************************/
const getMean = function(input) {
  return parseFloat((((input.map(function(i){ return i.temperature })).reduce( function(total, i) { return total + i })) / (parseFloat(input.length))).toFixed(2));
}

/*******************************************************************
/ getMedian
/ MEDIAN - is the value separating the higher half 
/          of a sorted data set 
/ from the lower half (ref. https://en.wikipedia.org/wiki/Median)
/ Note:
/   2 Cases assumed:
/     (1) - even number of elements in data set
/         When there is an even number of elements in a data set,
/         the median is the 'average' of the 'middle 2 elements'.
/     (2) - odd number of elements in data set
/        When there is an odd number of elements in a data set,
/        the median occurs exactly at the half of the 
/        sorted data set.
*******************************************************************/
const getMedian = function(input) {
  // SCALE AND SORT
  let scaledInput = (input.map(function(i){ 
    return parseInt((i.temperature * 100.0).toFixed(2));
  }));
  let sortedInput = scaledInput.sort(function(a, b) {
    return a - b;
  });
  if(sortedInput.length % 2 === 1) {
    return parseFloat(((sortedInput[(sortedInput.length - 1)/2]) / 100.0).toFixed(2));
  }
  else {
    const sum = parseFloat(((sortedInput[sortedInput.length/2] + sortedInput[(sortedInput.length/2) - 1]) / 100.0).toFixed(2));
    return parseFloat((sum / 2.0).toFixed(2));
  }
}

/*******************************************************************
/ getMode
/ MODE - The mode of a set of data values is the value 
/        that appears most often 
/        (ref. https://en.wikipedia.org/wiki/Mode_(statistics))
/ Note:
/  - It is assumed that there can be more than one MODE in a data set
/  - If all elements occur at the same rate, then the mode is
/    said to be non-existent (ref. http://www.purplemath.com/modules/meanmode.htm)
*******************************************************************/
const getMode = function(input) {
 // SCALE AND SORT
 let scaledInput = (input.map(function(i){ 
    return parseInt((i.temperature * 100.0).toFixed(2));
 }));
 let sorted = scaledInput.sort(function(a, b) {
    return a - b;
 });
 // INITIALIZE MODE TO FIRST ELEMENT
 let index = 0;
 let modes = [parseFloat((sorted[index] / 100.0).toFixed(2))];
 let currentModeCount = sorted.lastIndexOf(sorted[index]) + 1;
 index = currentModeCount;
 while(index < sorted.length) {
  let thisModeCount = sorted.lastIndexOf(sorted[index]) - sorted.indexOf(sorted[index]) + 1;
  if(thisModeCount > currentModeCount){
    // CHANGE CURRENT MODE
    const thisMode = parseFloat((sorted[index] / 100.0).toFixed(2));
    modes = [thisMode]
    currentModeCount = thisModeCount;
  }
  else if(thisModeCount === currentModeCount) {
    // PUSH TO MODE SINCE SAME COUNT
    const thisMode = parseFloat((sorted[index] / 100.0).toFixed(2));
    modes.push(thisMode);
  }
  else {
    // DO NOTHING
  }
  index = sorted.lastIndexOf(sorted[index]) + 1;
 }
 if(modes.length === input.length) return (null);
 else return modes;
}

// MODULE EXPORTS
exports.getMean = getMean;
exports.getMedian = getMedian;
exports.getMode = getMode;
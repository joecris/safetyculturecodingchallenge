/********************************************************************************************************
/* Author : Joe Cris Molina
/* Revision History : 
/*       June 20, 2018 (created)
/* Description :
/*  Utility functions
*********************************************************************************************************/

/*******************************************************************
/ CHECK INPUT
*******************************************************************/
const checkInput = function(input) {
  // CHECK 'id' and 'temperature' keys (expected to be present in all data point)
  if(input && input.length > 0) {
    
    if((input.map(function(i){
      return i.id
    }).filter(function(f) {
      return (f === null || f === undefined)
    }).length) > 0 )return ('INVALID OBJECT KEY')
    
    if((input.map(function(i){
      return i.temperature
    }).filter(function(f) {
      return (f === null || f === undefined)
    }).length) > 0) return ('INVALID OBJECT KEY')

    return (null);
  }
  else {
    if(input === undefined || input === null) return ('UNDEFINED INPUT')
    else if(input.length === 0) return ('EMPTY INPUT')
    else return ('INVALID INPUT');
  }
}

/*******************************************************************
/ getSensorIds
*******************************************************************/
const getSensorIds = function(input) {
  let ids = input.map( function(i) { return i.id} );
  return ids.filter( function (i, index) { return ids.indexOf(i) >= index });
}

// MODULE EXPORTS
exports.checkInput = checkInput;
exports.getSensorIds = getSensorIds;
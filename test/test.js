var assert = require('chai').assert;
var mainApp = require('../app.js').mainApp;

// RUN TESTS
describe('RUNNING TESTS...', () => {

  describe('testing against empty input file path', () => {
    it('SHOULD RETURN NULL OUTPUT, WHEN INPUT IS EMPTY FILE PATH', (done) => {
      var output = mainApp();
      console.log('[OUTPUT]');
      console.log(output);
      assert(output === null);
      done();
    })
  });

  describe('testing against empty json file', () => {
    it('SHOULD RETURN NULL OUTPUT, WHEN INPUT IS EMPTY JSON', (done) => {
      var output = mainApp('./inputs/empty.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output === null);
      done();
    })
  });

  describe('testing against invalid input', () => {
    it('SHOULD RETURN NULL OUTPUT, WHEN INPUT IS INVALID JSON', (done) => {
      var output = mainApp('./inputs/invalid.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output === null);
      done();
    })
  });

  describe('testing against single', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS EMPTY SINGLE ELEMENT JSON', (done) => {
      var output = mainApp('./inputs/input1.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

  describe('testing against random input (1)', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS RANDOM DATA SET WITH CORRECT FORMAT', (done) => {
      var output = mainApp('./inputs/input.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

  describe('testing against random input (2)', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS RANDOM DATA SET WITH CORRECT FORMAT', (done) => {
      var output = mainApp('./inputs/input2.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

  describe('testing against random input (3)', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS RANDOM DATA SET WITH CORRECT FORMAT', (done) => {
      var output = mainApp('./inputs/input3.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

  describe('testing against random input (4)', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS RANDOM DATA SET WITH CORRECT FORMAT', (done) => {
      var output = mainApp('./inputs/1529478447942.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

  describe('testing against random input (4) - integer input', () => {
    it('SHOULD RETURN CORRECT OUTPUT, WHEN INPUT IS RANDOM DATA SET WITH CORRECT FORMAT', (done) => {
      var output = mainApp('./inputs/integer.json');
      console.log('[OUTPUT]');
      console.log(output);
      assert(output !== null);
      done();
    })
  });

});
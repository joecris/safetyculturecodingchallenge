// GENERATE 3000 random data set points
const MAX_DATA_SET_POINTS = 3000;
const VALID_SENSOR_IDS = ['a', 'b', 'c', 'd'];

const getRandomInt = function(max) {
  return Math.floor(Math.random() * Math.floor(max));
};

generateRandomFloat = function() {
  return parseFloat((Math.random() * 100.0 + Math.random()).toFixed(2));
};

var fs = require('fs');

var fileName = '../inputs/' + (new Date()).getTime() + '.json'
fs.appendFile(fileName, '[\n', () => { 

  for(var i=0; i<MAX_DATA_SET_POINTS; i++) {
    if(i != MAX_DATA_SET_POINTS - 1) fs.appendFile(fileName, '{"id": "' + VALID_SENSOR_IDS[getRandomInt(VALID_SENSOR_IDS.length)] + '", "timestamp" : 1, "temperature": ' + generateRandomFloat() +  '},\n', () => { });
    else fs.appendFile(fileName, '{"id": "' + VALID_SENSOR_IDS[getRandomInt(VALID_SENSOR_IDS.length)] + '", "timestamp" : 1, "temperature": ' + generateRandomFloat() +  '}\n', () => { 
      fs.appendFile(fileName, ']', () => { });
    });
  }
});

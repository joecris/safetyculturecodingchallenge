#SafetyCulture - Software Engineer Coding Challenge

##Joe Cris Molina (June 20, 2018)

## Steps to test and run the app

+ Make sure you have NodeJS installed (depending on your machine select the suitable NodeJS installer)
    - https://nodejs.org/en/download/
+ Get a copy of the code
    - 'git clone https://joecris@bitbucket.org/joecris/safetyculturecodingchallenge.git' 
+ Run 'npm install' to install MochaJS and ChaiJS which is needed to run the test scripts
+ Run tests using command 'npm run test'
+ How to use the main app module
    - the file app.js module exports a function named 'mainApp' which accepts a file path to a json file which contains the data set points (for reference and sample json file please see /inputs/input.json) and returns an array of JSON output.
    - to use the mainApp function from the app.js module we need to import and supply the file path input to the data set JSON file
    - then we can use plain old 'node [js file]' command
    - please refer to sampleApp.js 
    - to run sampleApp.js, use 'node sampleApp.js' command

## Input files

+ Under the folder named 'inputs' are some sample JSON files that conatins some data points
    - ex. input.json conatins the data points described in the SafetyCulture Software Engineer Coding Challenge.pdf
    - ex. 1529478447942.json conatains 3000 data points

## Notes

+ This app is written entirely indepenedent of external libraries (except for the test scripts where MochaJS was used)
+ As long as you have NodeJS (and NPM) installed, you are good to go! :D 

# Implementation Guidelines

+ Javascript (NodeJS) was used in implementing the solution, aside from this being the programming language I am most comfortable with, using Javascript (NodeJS) helps a lot in terms of how fast a solution can be developed and tested. Since we do not have to install and compile a lot.
+ Input method to the mainApp module is via JSON file (i.e. a file path should be supplied to the function after importing it)
+ To test and run the app, please refer to to top-most section
+ Assumptions made
    - MEAN - sum(elements) / total number of elements
    - MEDIAN - is the value separating the higher half of a sorted data set from the lower half (ref. https://en.wikipedia.org/wiki/Median). If the total number of element is even, the Final MEDIAN is the average of the '2 middle elements'.
    - MODE - The mode of a set of data values is the value that appears most often (ref. https://en.wikipedia.org/wiki/Mode_(statistics)). There can be more than one mode in a data set.
    - Data precision is up to 2 decimal places.
+ This application (except for the test scripts which requires MochaJS and ChaiJS) is written entirely in vanilla Javascript and does not depend on external libraries
+ Important design considerations include the separation of the statistical functions to isolate them, just in case a 'better/more efficient' implementation comes up, it will be easy to change and reintegrate this part. Although, these statistical functions may be further be improved by accepting purely an array of numbers (currently it accepts the JSON arrays). Also, 'scaling' was implemented under the hood in the statistical functions to avoid unwanted floating point bugs and nuisances during sorting etc. It also probably will make the program faster if the data set points was sorted at the beginning, instead of sorting it during getMedian() and again at getMode().
+ The test scripts may also be too basic and may be further improved (i.e. more robust input and output checking)
+ For questions and clarifications, please do not hesitate to email me @ joecris.molina@gmail.com.
+ Thanks, cheers!
/********************************************************************************************************
/* Author : Joe Cris Molina
/* Revision History : 
/*       June 20, 2018 (created)
/* Description :
/*  Get the MEAN, MEDIAN and MODE of an input set of 'fridge
/*  sensor data'
/* Notes:
/*  - Please refer to README.md for additional instructions
/*    on how to run this app.
/*  - This app is written in vanilla Javascript (without any dependency to external libraries)
/*  - You must have NodeJS installed to run this app
/*  - decimal precision is limited to 2 decimal places
*********************************************************************************************************/

/*******************************************************************
/ Input file path
*******************************************************************/
// const inputFilePath = process.env.INPUT_FILE;

// if(!inputFilePath || inputFilePath === '') {
//   console.log('INVALID INPUT FILE PATH');
//   return (null);
// }

// let input = null;
// try {
//   input = require(inputFilePath);
// }
// catch (e) {
//   console.log('ERROR READING INPUT FILE');
//   return (null);
// }

/*******************************************************************
/ Util functions
*******************************************************************/
const checkInput = require('./utils.js').checkInput;
const getSensorIds  = require('./utils.js').getSensorIds;

/*******************************************************************
/ Stat functions
*******************************************************************/
const getMean = require('./getStatistics.js').getMean;
const getMedian = require('./getStatistics.js').getMedian;
const getMode = require('./getStatistics.js').getMode;

/*******************************************************************
/ Main app
*******************************************************************/
const mainApp = function(inputFilePath) {
  // GET INPUT
  if(!inputFilePath || inputFilePath === '') {
    console.log('INVALID INPUT FILE PATH');
    return (null);
  }

  let input = null;
  try {
    input = require(inputFilePath);
  }
  catch (e) {
    console.log('ERROR READING INPUT FILE');
    return (null);
  }

  // SOME BASIC INPUT CHECKING
  const err = checkInput(input);
  if(err) { 
    console.log('[INVALID INPUT] ' + err); 
    return (null); 
  }

  // GET UNIQUE SENSOR IDs
  const sensorIds = getSensorIds(input);

  //FILTER INPUT BY SENSOR IDs
  let filteredInput = [];
  sensorIds.forEach(function(id) {
    filteredInput.push(input.filter(function(i){ return i.id === id}));
  });

  // GET MEAN, MEDIAN, MODE of FILTERED INPUT
  const output = filteredInput.map( function(input) {
    let output = {
      id: input[0].id,
      average: getMean(input.concat([])),
      median: getMedian(input.concat([])),
      mode: getMode(input.concat([]))
    }
    return(output)
  });

  //console.log(output); // LOG OUTPUT

  // RETURN OUTPUT
  return output;
}

//mainApp();

exports.mainApp = mainApp;